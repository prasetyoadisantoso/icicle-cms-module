<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Routing for CMS Dashboard */
Route::prefix('cms')->group(function() {
    Route::get('/', 'CMSController@index')->name('cms.index');
});


/* Routing for Posts */
Route::prefix('posts')->group(function() {
    Route::get('/', 'PostsController@index')->name('post.index');
    Route::post('images', 'PostsController@uploadImage')->name('post.image');
    Route::get('create', 'PostsController@create')->name('post.create');
    Route::post('store', 'PostsController@store')->name('post.store');
    Route::get('edit/{id}', 'PostsController@edit')->name('post.edit');
    Route::patch('update/{id}', 'PostsController@update')->name('post.update');
    Route::delete('delete/{id}', 'PostsController@destroy')->name('post.delete');
});


/* Routing for Categories */
Route::prefix('postcat')->group(function() {
    Route::get('/', 'PostcategoriesController@index')->name('postcat.index');
    Route::get('create', 'PostcategoriesController@create')->name('postcat.create');
    Route::post('store', 'PostcategoriesController@store')->name('postcat.store');
    Route::get('edit/{id}', 'PostcategoriesController@edit')->name('postcat.edit');
    Route::patch('update/{id}', 'PostcategoriesController@update')->name('postcat.update');
    Route::delete('delete/{id}', 'PostcategoriesController@destroy')->name('postcat.delete');
});
