<?php

namespace Modules\CMS\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\CMS\Entities\Categories;
use Modules\CMS\Entities\CategoryPosts;
use Modules\CMS\Http\Controllers\Controller;
use Modules\CMS\Entities\Posts;
use Modules\Administrator\Classes\ReadModuleClass;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{

    /* roles permissions variable */
    public $roles;

    /* Read Module Variable */
    public $modules;

    public function __construct()
    {
        /* Access Permission Post */
        $this->middleware('permission:post-list')->only(['index']);
        $this->middleware('permission:post-create')->only(['create']);
        $this->middleware('permission:post-edit')->only(['edit']);
        $this->middleware('permission:post-delete')->only(['destroy']);

        $this->middleware(['auth', 'verified']);
        $this->roles = Auth::user();
        $this->modules = ReadModuleClass::read();
    }







    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        // Show Data Posts
        $category = Posts::with('categories')->orderBy('id', 'DESC')->paginate(5);

        view()->share('module', $this->modules);

        return view('cms::posts.index', compact('category'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }






    /**
     * Show form creating post
     * @return Renderable
     */
    public function create()
    {

        $categories = Categories::all();

        view()->share('module', $this->modules);

        return view('cms::posts.create', compact('categories'));
    }







    /**
     * Store a newly created resource in storage and database
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {

        /* Validating the request */
        $this->validate($request, [
            'title' => 'required|string',
            'categories' => 'required',
            'content' => 'required'
        ]);


        /* Save to Posts database */
        Posts::create([
            'title' => $request->title,
            'content' => $request->content
        ]);


        /* Get latest id of posts and incerement 1 number */
        $latest_id = Posts::latest()->first();


        /* Save Category to category_posts table */
        $latest_id->categories()->attach($request->categories);




        return redirect()->route('post.index')->with(['success' => 'Article has been created']);
    }








    /**
     * Show the detail of post, but i think this is next feature
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('cms::show');
    }






    /**
     * Show the form for editing from post
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {

        /* Get Post with relation Categories */
        $data = Posts::with('categories')->findOrFail($id);

        /* get categories and save to an array */
        foreach ($data->categories as $key => $value) {
            $arr[] = $value->id;
        }

        view()->share('module', $this->modules);

        /**
         * Exception handling but with if statement
         */
        if (isset($arr)) {
            # code...
            $categories = Categories::all();
            return view('cms::posts.edit', compact('data', 'categories', 'arr'));
        } else {
            $categories = Categories::all();
            return view('cms::posts.edit', compact('data', 'categories'));
        }
    }






    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        /* Validating from request */
        $this->validate($request, [
            'title' => 'required|string',
            'categories' => 'required',
            'content' => 'required'
        ]);

        /* Get id from Post */
        $data = Posts::find($id);

        /* Bulk Update all Post content */
        $data->update($request->all());

        /* Update categories with its post */
        $data->categories()->sync($request->input('categories'));

        /* Redirect with success Message */
        return redirect()->route('post.index')->with('success', 'Article has been updated');
    }







    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        /* Get data from selected Post */
        $data = Posts::find($id);

        /* Detach Category of Posts */
        $data->categories()->detach();

        /* Delete Data */
        $data->delete();

        /* Return Success */
        return redirect()->route('post.index')
            ->with('success', 'Article deleted successfully');
    }






    /**
     * Upload Image with CKEditor
     * @return response
     */
    public function uploadImage(Request $request)
    {
        /* Check any image that exist , sent by user */
        if ($request->hasFile('upload')) {
            $file = $request->file('upload');
            $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName . '_' . time() . '.' . $file->getClientOriginalExtension();

            $file->move(public_path('uploads'), $fileName);

            $ckeditor = $request->input('CKEditorFuncNum');
            $url = asset('uploads/' . $fileName);
            $msg = 'Image uploaded successfully';

            $response = "<script>window.parent.CKEDITOR.tools.callFunction($ckeditor, '$url', '$msg')</script>";

            /* Set the header */
            @header('Content-type: text/html; charset=utf-8');
            return $response;
        }
    }
}
