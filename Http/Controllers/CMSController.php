<?php

namespace Modules\CMS\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\CMS\Classes\PostClass;
use Illuminate\Support\Facades\Auth;
use Modules\Administrator\Classes\ReadModuleClass;
use Modules\CMS\Classes\CategoriesClass;

class CMSController extends Controller
{

    // roles permissions function
    public $roles;

    // Read Modules
    public $modules;

    // Read Posts
    public $posts;
    public $latestPosts;

    // Read Categories
    public $categories;
    public $latest_category;

    public function __construct()
    {
        /* Permission Access to CMS */
        $this->middleware('permission:cms-access');
        $this->middleware(['auth','verified']);


        $this->roles = Auth::user();
        $this->modules = ReadModuleClass::read();
        $this->posts = PostClass::totalPosts();
        $this->categories = CategoriesClass::totalCategories();
        $this->latestPosts = PostClass::latestPost();
        $this->latest_category = CategoriesClass::latestCategories();
    }


    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {

        $total = PostClass::totalPosts();

        /**
         * Total Modules
         */
        view()->share('module', $this->modules);


        /**
         * Total Posts
         */
        view()->share('post', $this->posts);


        /**
         * Total Categories
         */
        view()->share('category', $this->categories);

        /**
         * Latest Posts
         */
        view()->share('latestPost', $this->latestPosts);

        /**
         * Latest Categories
         */
        view()->share('latest_category', $this->latest_category);


        return view('cms::dashboard.index', compact('total'));
    }

}
