<?php

namespace Modules\CMS\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\CMS\Entities\Categories;
use Modules\Administrator\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Administrator\Classes\ReadModuleClass;

class PostcategoriesController extends Controller
{
    /* roles permissions variable */
    public $roles;

    /* Read Modules Variable */
    public $modules;

    public function __construct()
    {
        /* Access Permission Post */
        $this->middleware('permission:category-list')->only(['index']);
        $this->middleware('permission:category-create')->only(['create']);
        $this->middleware('permission:category-edit')->only(['edit']);
        $this->middleware('permission:category-delete')->only(['destroy']);

        $this->middleware(['auth', 'verified']);
        $this->roles = Auth::user();
        $this->modules = ReadModuleClass::read();
    }





    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        /* Get Categories data and show it */
        $data = Categories::orderBy('id', 'DESC')->paginate(5);

        view()->share('module', $this->modules);

        /* Share to the view */
        return view('cms::categories.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }




    /**
     * Show form create post
     * @return Renderable
     */
    public function create()
    {
        view()->share('module', $this->modules);

        /* Go to view */
        return view('cms::categories.create');
    }





    /**
     * Store a newly created resource in storage and database
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        /* Validating request */
        $detail = request()->validate([
            'category' => 'required',
            'description' => 'required',
        ]);

        /* Save data category */
        $form = new Categories();
        $form->category = $detail['category'];
        $form->description = $detail['description'];
        $form->save();

        /* Redirect to category index with success messages */
        return redirect()->route('postcat.index')
            ->with('success', 'Category has been created');
    }






    /**
     * Show the detail category but i think for next feature
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {

        view()->share('module', $this->modules);

        return view('cms::show');
    }






    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        /* Get data from selected category  */
        $data = Categories::find($id);

        view()->share('module', $this->modules);

        /* Share data to view */
        return view('cms::categories.edit',  compact('data'));
    }








    /**
     * Update the specified resource in storage and database
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {

        /* Validate Request */
        $this->validate($request, [
            'category' => 'required',
            'description' => 'required'
        ]);

        /* Get data of categories */
        $data = Categories::find($id);

        /* Bulk update categories */
        $data->update($request->all());

        /* Redirect to categories Index with success message */
        return redirect()->route('postcat.index')->with('success', 'Category has been updated');
    }







    /**
     * Remove the specified resource from storage and database
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        /* Get data from categories */
        $data = Categories::find($id);

        /* Detach posts with catagories */
        $data->posts()->detach();

        /* Delete data */
        $data->delete();

        /* Redirect to category index with success message */
        return redirect()->route('postcat.index')
            ->with('success', 'Category deleted successfully');
    }
}
