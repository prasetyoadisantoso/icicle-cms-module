<?php

namespace Modules\CMS\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;


class AccessCMSTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Insert list of permissions here
        $permissions = [
            'cms-access',
            'post-list',
            'post-create',
            'post-edit',
            'post-delete',
            'category-list',
            'category-create',
            'category-edit',
            'category-delete',
         ];


         foreach ($permissions as $permission) {
              Permission::create(['name' => $permission]);
         }
    }
}
