<?php
namespace Modules\CMS\Classes;

use Illuminate\Support\Facades\View;
use Modules\CMS\Entities\Categories;
use Str;

class CategoriesClass {

    // Get Posts content
    public static function getCategories()
    {
        $categories = Categories::with('posts')->orderBy('id','DESC')->paginate(3);
        return $categories;
    }

    // Get Posts total
    public static function totalCategories()
    {
        $total = Categories::get()->count();
        return $total;
    }

    // Get Latest Posts
    public static function latestCategories()
    {
        $latest_categories = Categories::orderBy('id','DESC')->get();

        foreach ($latest_categories as $item) {
            # code...
            $latest_category = $item->category;

            return Str::limit($latest_category, 10);
        }

    }

}
