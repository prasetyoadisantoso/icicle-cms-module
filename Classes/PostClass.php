<?php
namespace Modules\CMS\Classes;

use Illuminate\Support\Facades\View;
use Modules\CMS\Entities\Posts;
use Illuminate\Support\Str;

class PostClass {

    // Get Posts content
    public static function getPosts()
    {
        $post = Posts::with('categories')->orderBy('id','DESC')->paginate(3);

        return $post;
    }

    // Get Posts total
    public static function totalPosts()
    {
        $total = Posts::get()->count();
        return $total;
    }

    // Get Latest Posts
    public static function latestPost()
    {
        $latest_post = Posts::orderBy('id','DESC')->get();

        foreach ($latest_post as $item) {
            # code...
            $latestPosts = $item->title;

            return Str::limit($latestPosts, 10);
        }

    }


}
