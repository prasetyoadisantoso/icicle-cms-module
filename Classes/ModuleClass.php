<?php
namespace Modules\CMS\Classes;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

class ModuleClass {

    use HasRoles;

    /* Logout Function */
    public static function read()
    {
        # code...
        $json = file_get_contents(base_path('Modules/CMS/module.json'));
        $jsonLow = strtolower($json);
        $string = json_decode($jsonLow, true);
        $modules = collect($string);
        $module = $modules['name'];
        return $module;
    }

}
