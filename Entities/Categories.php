<?php

namespace Modules\CMS\Entities;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $fillable = ['category', 'description'];

    // public function category_posts()
    // {
    //     return $this->hasMany('Modules\CMS\Entities\CategoryPosts');
    // }

    public function posts()
    {
        return $this->belongsToMany('Modules\CMS\Entities\Posts', 'category_posts');
    }
}
