<?php

namespace Modules\CMS\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    // use HasFactory;
    protected $primaryKey = 'id';

    protected $fillable = ['title', 'content'];


    public function categories()
    {
        return $this->belongsToMany('Modules\CMS\Entities\Categories', 'category_posts');
    }

}
