<?php

namespace Modules\CMS\Entities;

use Illuminate\Database\Eloquent\Model;

class CategoryPosts extends Model
{
    protected $fillable = ['posts_id', 'categories_id'];

    public function posts()
    {
        return $this->belongsTo('Modules\CMS\Entities\Posts');
    }

    public function categories()
    {
        return $this->belongsTo('Modules\CMS\Entities\Categories');
    }


}
