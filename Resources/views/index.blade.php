@extends('adminlte.layouts.master')

@section('content-dashboard')
<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-olive navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-sm-inline-block">
            <a href="{{url('/')}}" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-sm-inline-block">
            <a href="{{route('logout')}}" class="nav-link">Logout</a>
        </li>
    </ul>
</nav>
<!-- /.navbar -->

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-olive elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
        <img src="{{asset('assets/images/icicle-logo.png')}}" alt="Icicle" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light ml-3">{{config('app.name')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('assets/images/user.png')}}" class="img-circle elevation-2"
                alt="User Image">
            </div>
            <div class="info">
                <a href="" class="d-block">{{$user->name}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a href="{{route('administrator')}}" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>

                <li class="nav-header">CMS Modules Installed</li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link text-light">
                        <i class="fas fa-book mr-2"></i>
                        <p>
                            CMS Menu
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('cms.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>CMS Dashboard</p>
                            </a>
                        </li>

                        @can('category-list')
                        <li class="nav-item">
                            <a href="{{route('postcat.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Categories</p>
                            </a>
                        </li>
                        @endcan

                        @can('post-list')
                        <li class="nav-item">
                            <a href="{{route('post.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Posts</p>
                            </a>
                        </li>
                        @endcan
                    </ul>

                </li>

                <div class="dropdown-divider"></div>

                <li class="nav-header">Settings and Cache Management</li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-laptop"></i>
                        <p>
                            Settings
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('setting-access')
                        <li class="nav-item">
                            <a href="{{route('settings')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Setting</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('cache')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Cache</p>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </li>



                <li class="nav-header">User and Permission Management</li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            User Management
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('user-list')
                        <li class="nav-item">
                            <a href="{{route('users')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Users</p>
                            </a>
                        </li>
                        @endcan

                        @can('role-list')
                        <li class="nav-item">
                            <a href="{{route('roles')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Roles</p>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">


    {{-- Start Dashboard CMS Section --}}
    @yield('dashboard-index')


    {{-- Start Roles Section --}}
    {{-- Index Roles --}}
    @yield('role-index')
    {{-- Create Roles --}}
    @yield('role-create')
    {{-- Edit Roles --}}
    @yield('role-edit')
    {{-- Show Roles --}}
    @yield('role-show')
    {{-- End Roles Section --}}



    {{-- Start User Section --}}
    {{-- Index User --}}
    @yield('user-index')
    {{-- Index User --}}
    @yield('user-create')
    {{-- Edit User --}}
    @yield('user-edit')
    {{-- Show User --}}
    @yield('user-show')


    {{-- Start Settings Section --}}
    {{-- Index Setting --}}
    @yield('setting-index')
    {{-- Index Cache --}}
    @yield('cache-index')
    {{-- End Setting Section --}}


    {{-- Start Post Section --}}
    {{-- Index Post --}}
    @yield('post-index')
    {{-- Index Post --}}
    @yield('post-create')
    {{-- Edit Post --}}
    @yield('post-edit')
    {{-- Show User --}}
    @yield('post-show')

    {{-- Start Post Categories Section --}}
    {{-- Index Post Categorie --}}
    @yield('postcat-index')
    {{-- Index Post Categorie --}}
    @yield('postcat-create')
    {{-- Edit Post Categoriet --}}
    @yield('postcat-edit')
    {{-- Show Post Categorie --}}
    @yield('postcat-show')

</div>


<!-- /.content-wrapper -->
<footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="http://prasetyoadisantoso.com" class="text-success">Pras Web
            Developer</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version Stable</b> | Stable-1.0.2
    </div>
</footer>

@endsection
