@extends('cms::index')
@section('dashboard-index')


<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">CMS Dashboard</h1>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('cms.index')}}">Home</a></li>
                    <li class="breadcrumb-item active">Content Management System Dashboard</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->




<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-12 col-xs-12 col-lg-12">
                <div class="jumbotron bg-success disabled">
                    <h1 class="display-5">Welcome to Content Management System Section </h1>
                    <p class="lead"> Manage your media, post, category and all of content whatever you want</p>
                    <hr class="my-4">
                    <p>Technology that i used</p>
                    <ul>
                        <li>Select2 </li>
                        <li>Ckeditor</li>
                    </ul>
                </div>
            </div>

            <div class="col-12">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-lg-3">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{$post}}</h3>

                                <p>Post</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-bullhorn"></i>
                            </div>
                            <a href="{{route('post.index')}}" class="small-box-footer">More info <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-sm-12 col-xs-12 col-lg-3">
                        <!-- small box -->
                        <div class="small-box bg-teal">
                            <div class="inner">
                                <h3>{{$category}}</h3>

                                <p>Category</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-book"></i>
                            </div>
                            <a href="{{route('postcat.index')}}" class="small-box-footer">More info <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-sm-12 col-xs-12 col-lg-3">
                        <!-- small box -->
                        <div class="small-box bg-orange">
                            <div class="inner">
                                <h3>{{$latestPost}}</h3>

                                <p>is your latest post</p>
                            </div>
                            <div class="icon">
                                <i class="far fa-clock"></i>
                            </div>
                            <a href="{{route('post.index')}}" class="small-box-footer">More info <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-sm-12 col-xs-12 col-lg-3">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3>{{$latest_category}}</h3>

                                <p>is your latest category</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <a href="{{route('postcat.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>


            </div>
        </div>
    </div>
</section>






@endsection
