@extends('cms::index')
@section('post-edit')



<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Edit Post</h1>
                <small>&nbsp; Edit your post</small>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('postcat.index')}}">Home</a></li>
                    <li class="breadcrumb-item active">EditPost</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->




{{-- Message Error --}}
@if (count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif




<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-8 col-lg-8">
                <div class="card">




                    <div class="card-header d-flex">
                        <div class="pull-left">
                            <h3>Edit Post</h3>
                        </div>
                    </div>




                    {{-- CKEditor Section --}}

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card-body">

                                @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                                @endif



                                <form action="{{ url('posts/update/'. $data->id) }}" method="post">
                                    @method('PATCH')
                                    @csrf

                                    <div class="form-group">
                                        <label for="">Title</label>
                                        <input type="text" name="title" class="form-control" value="{{ $data->title }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="">Categories</label><br>
                                        <select multiple class="form-control selectpicker"
                                            id="exampleFormControlSelect2" name="categories[]" style="width: 100%">
                                            @foreach ($categories as $items)

                                            @if (isset($arr))
                                            <option {{ in_array($items->id, $arr) == $items->id ? 'selected' : '' }}
                                                value="{{$items->id}}">{{ $items->category }}</option>
                                            @else
                                            <option value="{{$items->id}}">{{ $items->category }}</option>

                                            @endif

                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Content</label>
                                        <textarea type="text" name="content" id="contents"
                                            class="form-control">{{ $data->content }}</textarea>
                                    </div>



                                    <button type="submit" class="btn btn-success"><i
                                            class="fas fa-save mr-2"></i>Save</button>
                                    <a class="btn btn-secondary" href="{{ route('post.index') }}"><i
                                            class="fas fa-undo mr-2"></i> Back</a>

                                </form>






                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
</section>



{{-- end CKEditor --}}
<script src="{{asset('assets/AdminLTE/js/ckeditor/ckeditor.js')}}"></script>
<script>
    CKEDITOR.replace('contents', {
            filebrowserUploadUrl: "{{route('post.image', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
</script>

{{-- Multiselect Jquery --}}
<script src="{{asset('assets/AdminLTE/js/jquery-2.0.2.min.js')}}"></script>
<script src="{{asset('assets/multiselect/select2.min.js')}}"></script>
<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.selectpicker').select2();
    });
</script>


@endsection
