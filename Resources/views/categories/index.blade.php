@extends('cms::index')
@section('postcat-index')



<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Categories Management</h1>
                <small>&nbsp; List of categories of posts</small>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('postcat.index')}}">Home</a></li>
                    <li class="breadcrumb-item active">Create New Categories</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section>
    @if ($message = Session::get('success'))
    <div class="alert alert-success mx-5 alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <p>{{ $message }}</p>
    </div>
    @endif
</section>





<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">




                    <div class="card-header d-flex">
                        <div class="pull-left">
                            <h3>List of Categories</h3>
                        </div>

                        {{-- Button Create Roles --}}
                        <div class="ml-auto">
                            @can('category-create')
                            <a class="btn btn-outline-success" href="{{route('postcat.create')}}">
                                <i class="fas fa-user-edit mr-2"></i> Create New Category</a>
                            @endcan
                        </div>
                    </div>




                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            {{-- Responsive Table --}}



                            <div class="box-body table-responsive">
                                <table id="example1" class="example1 table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Category</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach ($data as $key => $category)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $category->category }}</td>
                                            <td>
                                                @can('category-edit')
                                                <a class="btn btn-primary"
                                                    href="{{ route('postcat.edit', $category->id) }}">Edit</a>
                                                @endcan
                                                {!! Form::open(['method' => 'DELETE','url' => ['postcat/delete',
                                                $category->id],'style'=>'display:inline'])!!}
                                                @can('category-delete')
                                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                @endcan
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{$data->render()}}
                            </div>



                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</section>


@endsection
